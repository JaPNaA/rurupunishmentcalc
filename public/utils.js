/**
 * Checks if an element is a decendent of another
 * @param {Element | EventTarget | ChildNode} child child ancestor
 * @param {Element | EventTarget | ChildNode} parent parent ancestor
 * @returns {boolean} is decendent?
 */
function isDecendent(child, parent) {
    /** @type {Element | null} */
    // @ts-ignore
    let curr = child;

    while (curr && curr.parentElement) {
        if (curr === parent) {
            return true;
        }

        curr = curr.parentElement;
    }

    return false;
}

/**
 * Callback for getFile
 * @callback getFileCallback
 * @param {Object} json
 * @returns {void}
 */

/**
 * @param {string} path
 * @param {getFileCallback} callback
 * @param {function} failCallback
 */
function getJSON(path, callback, failCallback) {
    let req = new XMLHttpRequest();
    req.responseType = "json";

    req.addEventListener("load", function () {
        let response;

        if (typeof req.response === "string") {
            try {
                response = JSON.parse(req.response);
            } catch (err) {
                failCallback();
            }
        } else if (req.response === null) {
            failCallback();
            return;
        } else {
            response = req.response;
        }

        callback(response);
    });

    req.open("GET", path);
    req.send();
}

/**
 * @param {string[]} arr 
 * @returns {string}
 */
function arrayToListSentence(arr) {
    let strBuff = [];
    for (let i of arr) {
        strBuff.push(i);
        strBuff.push(", ");
    }
    strBuff.pop();
    strBuff.splice(strBuff.length - 1, 0, " and ");
    return strBuff.join("");
}