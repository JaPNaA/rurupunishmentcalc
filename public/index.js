let site = new Site(restartFunc);

/**
 * Restarts the site
 * @param {*} nextStateValue 
 */
function restartFunc(nextStateValue) {
    site = new Site(restartFunc, nextStateValue);
}