class Calculator {
    constructor(options) {
        this.legacyFormulaMode = Boolean(options.legacyFormulaMode);

        this.defaultNumberOfRURUs = 15;

        if (this.legacyFormulaMode) {
            this.defaultFactor = 1.6;
        } else {
            this.defaultFactor = 34;
        }

        /** @type {HTMLDivElement} */
        // @ts-ignore
        this.$inputs = document.getElementById("inputs");

        /** @type {HTMLInputElement} */
        // @ts-ignore
        this.$numberOfRurus = document.getElementById("numberOfRurus");

        /** @type {HTMLInputElement} */
        // @ts-ignore
        this.$factor = document.getElementById("factor");

        /** @type {HTMLInputElement} */
        // @ts-ignore
        this.$result = document.getElementById("result");

        /** @type {HTMLButtonElement} */
        // @ts-ignore
        this.$add = document.getElementById("add");

        /** @type {HTMLButtonElement} */
        // @ts-ignore
        this.$clear = document.getElementById("clear");

        /** @type {HTMLInputElement[]} */
        this.inputs = [].slice.call(this.$inputs.querySelectorAll("input"));

        this.updateValues = this.updateValues.bind(this);
        this.createRuruField = this.createRuruField.bind(this);
        this.clearRURUs = this.clearRURUs.bind(this);
        this.newRURUHotkeyHandler = this.newRURUHotkeyHandler.bind(this);

        this.$numberOfRurus.addEventListener("input", this.updateValues);
        this.$factor.addEventListener("input", this.updateValues);
        this.$add.addEventListener("click", this.createRuruField);
        this.$clear.addEventListener("click", this.clearRURUs);

        this.$inputs.addEventListener("input", this.updateValues);
        this.$inputs.addEventListener("keydown", this.newRURUHotkeyHandler);

        if (!options.noClearRURUs) {
            this.clearRURUs();
        } else if (options.butDoSetDefaultValues) {
            this.setDefaultValues();
        }

        this.updateValues();
    }

    stop() {
        this.$numberOfRurus.removeEventListener("input", this.updateValues);
        this.$factor.removeEventListener("input", this.updateValues);
        this.$add.removeEventListener("click", this.createRuruField);
        this.$clear.removeEventListener("click", this.clearRURUs);

        this.$inputs.removeEventListener("input", this.updateValues);
        this.$inputs.removeEventListener("keydown", this.newRURUHotkeyHandler);
    }

    createRuruField() {
        const div = document.createElement("div");
        const input = document.createElement("input");

        input.type = "number";

        div.classList.add("ruru");

        div.appendChild(input);
        this.$inputs.appendChild(div);

        this.inputs.push(input);
        input.focus();

        return input;
    }

    updateValues() {
        let rurusBroken = [];

        let numberOfRurus = parseInt(this.$numberOfRurus.value);
        if (!numberOfRurus) {
            this.$numberOfRurus.classList.add("invalid");
        } else {
            this.$numberOfRurus.classList.remove("invalid");
        }

        let factor = parseFloat(this.$factor.value);
        if (!factor) {
            this.$factor.classList.add("invalid");
        } else {
            this.$factor.classList.remove("invalid");
        }

        for (let input of this.inputs) {
            let val = parseInt(input.value);

            if (val) {
                if (val < 1 || val > numberOfRurus) {
                    input.classList.add("invalid");
                } else {
                    input.classList.remove("invalid");
                    rurusBroken.push(val);
                }
            } else {
                if (input.value !== "") {
                    input.classList.add("invalid");
                }
            }
        }

        const rurusBrokenSorted = rurusBroken.sort(
            (x, y) => x - y
        );

        let result;
        if (this.legacyFormulaMode) {
            result = _old_calc(numberOfRurus, rurusBrokenSorted, factor);
        } else {
            result = calc(numberOfRurus, rurusBroken, factor);
        }

        this.$result.innerText = result ? result.toString() : "no";
    }

    /**
     * Handle tabs
     * @param {KeyboardEvent} e event data
     * @this {HTMLDivElement}
     */
    newRURUHotkeyHandler(e) {
        // Not tab or space or holding down any modifier keys
        if (
            e.keyCode !== 9 && e.keyCode !== 13 ||
            e.ctrlKey || e.altKey || e.shiftKey
        ) { return; }

        if (isDecendent(e.target, this.$inputs.lastChild)) {
            e.preventDefault();
            this.createRuruField();
        }
    }

    clearRURUs() {
        this.inputs.length = 0;
        while (this.$inputs.firstChild) {
            this.$inputs.removeChild(this.$inputs.firstChild);
        }

        this.setDefaultValues();
        this.createRuruField().value = this.defaultNumberOfRURUs.toString();
        this.createRuruField();
        this.updateValues();
    }

    setDefaultValues() {
        this.$numberOfRurus.value = this.defaultNumberOfRURUs.toString();
        this.$factor.value = this.defaultFactor.toString();
    }
}