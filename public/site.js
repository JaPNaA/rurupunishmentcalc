class Site {
    /**
     * @param {Function} restartFunc a function to restart the site
     * @param {*} [stateValue] a state to be in
     */
    constructor(restartFunc, stateValue) {
        this.stateValue = stateValue || {};
        this.restartFunc = restartFunc;

        /** @type {HTMLHeadingElement} */
        // @ts-ignore
        this.$mode = document.getElementById("mode");
        this.modeLegacy = document.getElementById("legacyFormulaMode");
        this.$modeCurrent = document.getElementById("currentFormulaMode");

        this.legacyFormulaMode = Boolean(this.stateValue.legacyFormulaMode);

        /** @type {HTMLDivElement} */
        // @ts-ignore
        this.$main = document.getElementById("main");

        /** @type {HTMLHeadingElement} */
        // @ts-ignore
        this.$title = document.getElementById("title");

        /** @type {HTMLDivElement} */
        // @ts-ignore
        this.$bird = document.getElementById("bird");

        this.possibleTitles = [
            "Lol someone broke a RURU",
            "What's the point of RURUs if there's no punishment?",
            "I'm typing this in class",
            "Do you like random titles?",
            "Jeeeeeeeeepsacar",
            "Knock knock. Who's there? FBI OPEN THE DOOR",
            "Debugging on mobile should be a joke",
            "Gitlab is underrated",
            "please use <b> bold </b> or <i> itallics </i> instead of underline.",
            "<a href=\"https://japnaa.github.io/\"> The best website </a>",
            "The FitnessGram&trade; Pacer Test is a multistage aerobic capacity test that progressively gets more difficult as it continues. The 20 meter pacer test will begin in 30 seconds. Line up at the start. The running speed starts slowly, but gets faster each minute after you hear this signal. [beep] A single lap should be completed each time you hear this sound. [ding] Remember to run in a straight line, and run as long as possible. The second time you fail to complete a lap before the sound, your test is over. The test will begin on the word start. On your mark, get ready, start.",
            "GRANDMA: PLAY DESPACITO!",
            "can we get an oof in the chat for Vladimir Doines?",
            "oof m8 u brokez a ruru YEET",
            "&gt;something referencing The Son&lt;",
            "they did surgery on a grape"
        ];

        this.calculator = new Calculator(this.stateValue);
        this.previousOffences = new PreviousOffences(this.stateValue);

        this.hideMain = this.hideMain.bind(this);
        this.unhideMain = this.unhideMain.bind(this);
        this.switchModes = this.switchModes.bind(this);

        if (!this.stateValue.noScrollTop) {
            document.documentElement.scrollTop = document.body.scrollTop = 0;
        }

        window.addEventListener("touchstart", this.hideMain);
        window.addEventListener("touchend", this.unhideMain);
        window.addEventListener("mousedown", this.hideMain);
        window.addEventListener("mouseup", this.unhideMain);

        this.$mode.addEventListener("click", this.switchModes);

        this.setRandomTitle();
        this.assignChildNumsToMainBoxes();
        this.updateModeElement();
    }

    /**
     * @param {*} [nextStateValue] 
     */
    restart(nextStateValue) {
        this.stop();
        this.calculator.stop();
        this.previousOffences.stop();
        this.restartFunc(nextStateValue);
    }

    stop() {
        window.removeEventListener("touchstart", this.hideMain);
        window.removeEventListener("touchend", this.unhideMain);
        window.removeEventListener("mousedown", this.hideMain);
        window.removeEventListener("mouseup", this.unhideMain);

        this.$mode.removeEventListener("click", this.switchModes);
    }

    /**
     * @param {Event} e
     */
    hideMain(e) {
        if (!(isDecendent(e.target, this.$main) || isDecendent(e.target, this.$bird))) {
            this.$main.classList.add("hide");
        }
    }

    unhideMain() {
        this.$main.classList.remove("hide");
    }

    setRandomTitle() {
        if (this.stateValue.noTitleChange) return;
        this.$title.innerHTML = this.possibleTitles[Math.floor(Math.random() * this.possibleTitles.length)];
    }

    assignChildNumsToMainBoxes() {
        let mainBoxes = document.getElementsByClassName("mainBox");

        for (let i = 0; i < mainBoxes.length; i++) {
            /** @type {HTMLElement} */
            // @ts-ignore
            const mainBox = mainBoxes[i];
            mainBox.style.setProperty("--child-num", i.toString());
        }
    }

    updateModeElement() {
        if (this.legacyFormulaMode) {
            this.$modeCurrent.classList.remove("selected");
            this.modeLegacy.classList.add("selected");
        } else {
            this.$modeCurrent.classList.add("selected");
            this.modeLegacy.classList.remove("selected");
        }
    }

    /**
     * @param {MouseEvent} event 
     */
    switchModes(event) {
        let useLegacyFormula = false;

        if (isDecendent(event.target, this.$modeCurrent)) {
            useLegacyFormula = false;
        } else if (isDecendent(event.target, this.modeLegacy)) {
            useLegacyFormula = true;
        } else {
            return;
        }

        if (useLegacyFormula == this.legacyFormulaMode) {
            return;
        }

        this.restart({
            noTitleChange: true,
            noUpdatePreviousOffences: true,
            noScrollTop: true,
            noClearRURUs: true,
            butDoSetDefaultValues: true,
            legacyFormulaMode: useLegacyFormula
        });
    }
}