/**
 * @param {Number} factor 
 * @param {Number} numRurus 
 * @param {Number} x 
 */
function _calcOneRuru(factor, numRurus, x) {
    const c = factor;
    const j = numRurus;
    return (0.5 * c + 0.4 * j) / (0.69 + Math.pow(Math.E, (x - 0.55 * j) / (2 + 0.1 * j)));
}

/**
 * Calculate number of hours for breaking s RURU
 * @param {Number} numberOfRurus number of RURUs at instant
 * @param {Number[]} rurusBroken all RURUs broken, by index, 1-indexed at instant
 * @param {Number} factor an arbitrary factor, can be adjusted to increase or decrease punishment
 * @returns {Number} hours to be demoted for
 */
function calc(numberOfRurus, rurusBroken, factor) {
    if (rurusBroken.length <= 0) { return 0; }
    
    let sum = 0;
    for (let i = 0; i < rurusBroken.length; i++) {
        sum += _calcOneRuru(factor, numberOfRurus, rurusBroken[i]);
    }
    return Math.round(sum);
}

/**
 * Calculate number of hours for breaking s RURU
 * @param {Number} numberOfRurus number of RURUs at instant
 * @param {Number[]} rurusBroken all RURUs broken, by index, 1-indexed at instant
 * @param {Number} factor an arbitrary factor, can be adjusted to increase or decrease punishment
 * @returns {Number} hours to be demoted for
 */
function _old_calc(numberOfRurus, rurusBroken, factor) {
    let sum = 0;

    if (rurusBroken.length <= 0) return 0;

    for (let i = 0; i < rurusBroken.length; i++) {
        let result = (numberOfRurus - rurusBroken[i]) * 2 / ((i * factor) + 1);
        sum += result;
    }
    return Math.round(sum + 4);
}