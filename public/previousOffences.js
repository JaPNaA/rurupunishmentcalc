class PreviousOffences {
    constructor(options) {
        this.options = options;

        /** @type {HTMLDivElement} */
        // @ts-ignore
        this.$previousOffencesList = document.getElementById("previousOffencesList");

        /** @type {HTMLDivElement} */
        // @ts-ignore
        this.$previousOffencesLastUpdated = document.getElementById("previousOffencesLastModified");

        this.initPreviousOffences();
    }

    stop() {
        // this class has no side-effects to take care of :D
    }

    initPreviousOffences() {
        if (this.options.noUpdatePreviousOffences) {
            return;
        }

        this.clearPreviousOffences();
        getJSON(
            "./previousOffences.json",
            this.createPreviousOffences.bind(this),
            this.failedLoadPreviousOffences.bind(this)
        );
    }

    clearPreviousOffences() {
        while (this.$previousOffencesList.firstChild) {
            this.$previousOffencesList.removeChild(
                this.$previousOffencesList.firstChild
            );
        }
    }

    /**
     * @param {Object} previousOffences
     */
    createPreviousOffences(previousOffences) {
        let lastUpdated = previousOffences.lastUpdated;
        let list = previousOffences.list;

        this.$previousOffencesLastUpdated.innerText =
            "Last updated: " + new Date(lastUpdated).toLocaleDateString();
        this.$previousOffencesList.appendChild(this.createPreviousOffencesList(list));
    }

    failedLoadPreviousOffences() {
        this.$previousOffencesLastUpdated.innerText = "Failed to load previous offences";
    }

    /**
     * @typedef {object} PreviousOffence
     * @property {string} offender the name of the offender
     * @property {array} rurusBroken the numbers of the broken RURUs
     * @property {number} numberOfRurus the number of RURUs in the document when it was broken
     * @property {number} [dateBroken] the day the RURU was broken
     * @property {number} result the number of hours the offender was demoted for
     */
    /**
     * @param {PreviousOffence[]} list
     * @returns {DocumentFragment}
     */
    createPreviousOffencesList(list) {
        let docFrag = document.createDocumentFragment();

        for (let offence of list) {
            docFrag.appendChild(this.createPreviousOffenceListElm(offence));
        }

        return docFrag;
    }

    /**
     * @param {PreviousOffence} previousOffence
     * @returns {HTMLDivElement}
     */
    createPreviousOffenceListElm(previousOffence) {
        let elm = document.createElement("div");
        elm.classList.add("previousOffence");

        {
            let offender = document.createElement("div");
            offender.classList.add("offender");
            offender.innerText = previousOffence.offender;
            elm.appendChild(offender);
        } {
            let rurusBroken = document.createElement("div");
            rurusBroken.classList.add("rurusBroken");
            rurusBroken.innerText = arrayToListSentence(previousOffence.rurusBroken);
            elm.appendChild(rurusBroken);
        } {
            let numberOfRurus = document.createElement("div");
            numberOfRurus.classList.add("numberOfRurus");
            numberOfRurus.innerText = previousOffence.numberOfRurus.toString();
            elm.appendChild(numberOfRurus);
        }
        if (previousOffence.dateBroken) {
            let dateBroken = document.createElement("div");
            dateBroken.classList.add("dateBroken");
            dateBroken.innerText = new Date(previousOffence.dateBroken).toLocaleDateString();
            elm.appendChild(dateBroken);
        }
        {
            let result = document.createElement("div");
            result.classList.add("result");
            result.innerText = previousOffence.result.toString();
            elm.appendChild(result);
        }

        return elm;
    }
}